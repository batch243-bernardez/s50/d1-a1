// import of the classes needed for the crc rule as well as the classes needed for the bootstrap component
import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){

	return(
		<Row className = "mt-3 mb-3"> 
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
				   	<Card.Body>
				    	<Card.Title>
				    		<h2>Learn From Home</h2>
				    	</Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porta elementum neque, ut egestas sapien. Vivamus ut dolor orci. Quisque at odio quam. Etiam suscipit lectus ut augue venenatis ornare. Praesent pharetra ante turpis, vitae lacinia enim porttitor at. Quisque vel dui volutpat lacus finibus finibus sed a lectus. Cras nibh purus, aliquet a ex non, tempus tristique tortor.
				        </Card.Text>
				   	</Card.Body>
				</Card>
			</Col>

			{/*This is the second card*/}
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
				   	<Card.Body>
				    	<Card.Title>
				    		<h2>Study Now, Pay Later</h2>
				    	</Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porta elementum neque, ut egestas sapien. Vivamus ut dolor orci. Quisque at odio quam. Etiam suscipit lectus ut augue venenatis ornare. Praesent pharetra ante turpis, vitae lacinia enim porttitor at. Quisque vel dui volutpat lacus finibus finibus sed a lectus. Cras nibh purus, aliquet a ex non, tempus tristique tortor.
				        </Card.Text>
				   	</Card.Body>
				</Card>
			</Col>

			{/*This is the third card*/}
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
				   	<Card.Body>
				    	<Card.Title>
				    		<h2>Be Part of our Community</h2>
				    	</Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porta elementum neque, ut egestas sapien. Vivamus ut dolor orci. Quisque at odio quam. Etiam suscipit lectus ut augue venenatis ornare. Praesent pharetra ante turpis, vitae lacinia enim porttitor at. Quisque vel dui volutpat lacus finibus finibus sed a lectus. Cras nibh purus, aliquet a ex non, tempus tristique tortor.
				        </Card.Text>
				   	</Card.Body>
				</Card>
			</Col>
		</Row>
		)
}