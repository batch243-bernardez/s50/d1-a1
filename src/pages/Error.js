import React from 'react';
import {Link} from 'react-router-dom';

export default function Error(){
	
	return(
		<div>
			<h3>Error: Page Not Found</h3>
			{"Go back to the "} 
			<Link to = "/">homepage.</Link>
		</div>
		)
}