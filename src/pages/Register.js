//Bootstrap grid system
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Container, Row, Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext'

import Swal from "sweetalert2";



export default function Register(){

	//State hooks to store the values of the input field from our user.
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	const [isActive, setIsActive] = useState(false);
	
	const {user, setUser} = useContext(UserContext);


	/*Business Logic
		- We want to disable the register button if one of the input fields is empty
	*/

	useEffect(() => {
		if(firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password1 !== "" && password2 !== "" && password1 === password2){
				setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password1, password2]);


	//This function will be triggered when the inputs in the Form will be submitted.

	function registerUser(event){
		event.preventDefault()
		
		fetch(`${process.env.REACT_APP_URI}/users/register`, {
			method: "POST",
			headers: {"Content-Type" : "application/json"},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo,
				email: email,
				password: password1,
				verifyPassword: password2
			})
		}).then(response => response.json())
		.then(data => {
			console.log(data);

			if(data.emailExists){
				Swal.fire({
					title: "Duplocate email found!",
					icon: "error",
					text: "Please use different email."
				})

				setPassword1("");
				setPassword2("");

			} else if (data.mobileLength === false){
				Swal.fire({
					title: "Invalid mobile number",
					icon: "error",
					text: "Mobile number must be at least 11 digits."
				})

				setPassword1("");
				setPassword2("");

			}
			else{
				Swal.fire({
					title: "Successfully registered!",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

				Navigate('/login');
			}
		})
	}


	return(

	(user.id !== null) ?
	<Navigate to = "/" />
	:

	<Container>
		<Row>
			<Col className = "col-md-4 col-8 offset-md-4 offset-2">
				<Form
					className = "bg-secondary p-3"
					onSubmit = {event => registerUser(event)}>
			      
			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>First Name</Form.Label>
			        <Form.Control
			        	type="name"
			        	placeholder="Enter First Name"
			        	value = {firstName}
			        	onChange = {event => setFirstName(event.target.value)}
			        	required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Last Name</Form.Label>
			        <Form.Control
			        	type="name"
			        	placeholder="Enter Last Name"
			        	value = {lastName}
			        	onChange = {event => setLastName(event.target.value)}
			        	required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="email">
			        <Form.Label>Email Address</Form.Label>
			        <Form.Control
			        	type="email"
			        	placeholder="Enter Email Address"
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required/>
			        {/*<Form.Text className="text-muted">
			          We'll never share your email with anyone else.
			        </Form.Text>*/}
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Enter your desired password</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Enter Password"
			        	value = {password1}
			        	onChange = {event => setPassword1(event.target.value)}
			        	required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Verify your password</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Verify Password"
			        	value = {password2}
			        	onChange = {event => setPassword2(event.target.value)}
			        	required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control
			        	type="number"
			        	placeholder="+63"
			        	value = {mobileNo}
			        	onChange = {event => setMobileNo(event.target.value)}
			        	required/>
			      </Form.Group>

			      <Button
			      	variant="primary"
			      	type="submit"
			      	disabled = {!isActive}>
			        Register
			      </Button>
			    </Form>
			</Col>
		</Row>
	</Container>		
		)
}