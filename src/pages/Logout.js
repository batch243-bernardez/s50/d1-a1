import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useContext, useEffect} from 'react';
import Swal from 'sweetalert2';

export default function Logout(){

	const{setUser, unsetUser} = useContext(UserContext);
	
	unsetUser();

	useEffect(() => {
		setUser({id: null, isAdmin: false})
	}, [])

	Swal.fire({
		title: "Welcome back to the outside world!",
		icon: "info",
		text: "Bumalik ka huh!!!"
	})

	return(
		<Navigate to = "/login" />
		)
}