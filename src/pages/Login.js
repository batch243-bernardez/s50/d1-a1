import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';

import userContext from '../UserContext'; 

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);
	/*const [user, setUser] = useState(null);*/

	//Allows us to consume the userContext object and its properties to use for the 
	const {user, setUser} = useContext (userContext)


	useEffect(() => {
		if(email !== "" & password !== ""){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password])


	function loginUser(event){
		event.preventDefault()
		/*alert("You are now logged in!")*/
		
		//Set the email of the authenticated user in the local storage
		//Syntax: localStorage.setItem("propertyName", value)
		
		//Storing information in the local storage will make the data persistent even if the page is refreshed, unlik with the use of states where information is reset when refreshing the page.
		/*localStorage.setItem("email", email);*/

		//Set the global user state to have properties obtained from the local storage
		/*setUser(localStorage.getItem("email"));

		setEmail("");
		setPassword("");*/

		// Process where it will fetch a request to the corresponding API
		// The header information "Content-Type" is used to specify that the information being sent to the backend will be sent to the form of json
		// The fetch request will communicate with our backend application providing it with a stringified json

		// Syntax: fetch('url', {options: method, headers, body})
		// .then(response => response.json())
		// .then(data => {data process})

		fetch(`${process.env.REACT_APP_URI}/users/login`, {
			method: "POST",
			headers: {'Content-Type' : 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.accessToken !== "empty"){
				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to our website!"
				})
			} else{
				Swal.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Please try again."
				})
				setPassword("")
			}
		})

		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_URI}/users/profile`,
				{headers: {
					Authorization: `Bearer ${token}`
				}})
			.then(response => response.json())
			.then(data => {
				console.log(data)
				
				setUser({id: data._id, isAdmin: data.isAdmin})
				console.log(user)
			})
		}
	}


	return(
		(user.id !== null)	?
		<Navigate to = "/" />
		:

	<Container>	
		<Row>
			<Col className = "col-md-4 col-8 offset-md-4 offset-2">
				<Form
					className = "p-3"
					onSubmit = {event => loginUser(event)}>
			      
			      <Form.Group className="mb-3" controlId="email">
			        <Form.Label>Email address:</Form.Label>
			        <Form.Control
			        	type="email"
			        	placeholder="Enter email"
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required/>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password:</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Password"
			        	value = {password}
			        	onChange = {event => setPassword(event.target.value)}
			        	required/>
			      </Form.Group>

			      <Button
			      	variant="success"
			      	type="submit"
			      	disbaled = {!isActive}>
			        Login
			      </Button>
			    </Form>
			</Col>
		</Row>
	</Container>	
		)
}