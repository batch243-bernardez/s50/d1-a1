import React from 'react';

//Create a context object
//A context object as the name state is a data type that can be used to store information that can be shared to other components within the app.
//The context object is a different approach to passing information between components and allows easier access between components and allows easier access by the use of prop-drilling.

//createContext is used to create a context in react
const UserContext = React.createContext();

//The provider component allows other component to consume or use the context objecy and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;