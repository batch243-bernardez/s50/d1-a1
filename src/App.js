/*import {Fragment} from 'react';*/
/*import './App.css';*/
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import {useState, useEffect} from 'react';

import AppNavBar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CourseView from './pages/CourseView';

import {UserProvider} from './UserContext'


function App() {

  //State hook for the user that will be globally accessible using the useContext. This will also be used to store the user information and be used for validating if a user is logged in on the app or not.
  const [user, setUser] = useState({id: null, isAdmin: false});
  
  //Function for clearing local storage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user)
  }, [user])

  useEffect(() => {
    fetch(`${process.env.REACT_APP_URI}/users/profile`,
      {headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }})
    .then(response => response.json())
    .then(data => {
      console.log(data)
      
      setUser({id: data._id, isAdmin: data.isAdmin})
      console.log(user)
    })
  }, [])

  return (
    //Router/BrowserRouter > Routes > Route
   <Router>
    <UserProvider value = {{user, setUser, unsetUser}}>
      <AppNavBar/>
        <Routes>
          <Route path = "/" element = {<Home/>}/>
          <Route path = "/courses" element = {<Courses/>}/>
          <Route path = "/courses/:courseId" element = {<CourseView/>}/>
          <Route path = "/login" element = {<Login/>}/>
          <Route path = "/register" element = {<Register/>}/>
          <Route path = "/logout" element = {<Logout/>}/>
          <Route path = "*" element = {<Error/>}/>
        </Routes>
    </UserProvider>    
   </Router>
  );
}

export default App;
